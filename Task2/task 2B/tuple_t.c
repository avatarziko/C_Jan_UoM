#include "tuple_t.h"

tuple_t *tuples;
int lengthTuples = 0;
int AmountTuples = 0;

typed typeDigit = d;
typed typeLong = l;
typed typeFloat = f;
typed typeDouble = b;
typed typeChar = c;
typed typeString = s;


static void printTuple(int i);

static char *formatInput(char userInput[SIZE]);

static int sizeInit(char *input);

static char *removeLastChar(char *string);

static char *fillTypes(tuple_t *ptr1, tuple_t *ptr2);

static char *fillTuple(tuple_t *ptr1);

static bool startsWith(const char *pre, const char *str);

static void error(int i);

static void ptr_valid(void *ptr);

static void funcStatus(int i);


void createTuple(char id[SIZE], char *userInput) {

    int overwriteFlag = 0; //A flag that is 1 if the tuple is going to be an Overwrite

    if (getTupleByID(id) != NULL) {
        printf("Tuple with id [%s] already exists [Overwritting]\n", id);
        deleteTuple(id);
        overwriteFlag = 1;
    }

    int tupleCount = lengthTuples;
    int j = 0;
    char *inputT;
    int tupleSize = 0;

    if (isdigit(userInput[0]) != 0) {
        inputT = formatInput(userInput);
        tupleSize = sizeInit(userInput);
    } else {
        printf("INPUT ERROR: First char has to be Amount of Vars in Tuple. Has to be a correct value.");
    }


    tuples = realloc(tuples, (tupleCount + tupleSize) * sizeof(tuple_t));
    ptr_valid(tuples);


    for (int i = tupleCount; i < tupleCount + tupleSize; ++i) {

        strcpy(tuples[i].id, id);
        tuples[i].size = tupleSize;
        tuples[i].head = tupleCount;

        if (inputT[j] == 'd') {

            int check = 0;
            char numstr[SIZE];
            int num = 0;

            do {
                printf("Enter Int Value");
                scanf("%s", numstr);
                if (sscanf(numstr, "%d", &num)) {
                    printf("You entered %d\n", num);
                    check = 0;
                } else {
                    printf("Invalid input '%s'\n", numstr);
                    check = -1;
                }
            } while (check == -1);

            tuples[i].data.val.d = num;
            tuples[i].data.type = typeDigit;

        } else if (inputT[j] == 'c') {

            puts("Enter Single Char:");
            scanf(" %c", &tuples[i].data.val.c);
            tuples[i].data.type = typeChar;

        } else if (inputT[j] == 'l') {

            int check = 0;
            char numstr[SIZE];
            long num = 0;

            do {
                printf("Enter Long Value:");
                scanf("%s", numstr);
                if (sscanf(numstr, "%ld", &num)) {
                    printf("You entered %ld\n", num);
                    check = 0;
                } else {
                    printf("Invalid input '%s'\n", numstr);
                    check = -1;
                }
            } while (check == -1);

            tuples[i].data.val.l = num;
            tuples[i].data.type = typeLong;

        } else if (inputT[j] == 'f') {

            int check = 0;
            char numstr[SIZE];
            float num = 0;

            do {
                printf("Enter Float Value:");
                scanf("%s", numstr);
                if (sscanf(numstr, "%f", &num)) {
                    printf("You entered %f\n", num);
                    check = 0;
                } else {
                    printf("Invalid input '%s'\n", numstr);
                    check = -1;
                }
            } while (check == -1);

            tuples[i].data.val.f = num;
            tuples[i].data.type = typeFloat;

        } else if (inputT[j] == 'b') {

            int check = 0;
            char numstr[SIZE];
            double num = 0;

            do {
                printf("Enter Double Value:");
                scanf("%s", numstr);
                if (sscanf(numstr, "%lf", &num)) {
                    printf("You entered %lf\n", num);
                    check = 0;
                } else {
                    printf("Invalid input '%s'\n", numstr);
                    check = -1;
                }
            } while (check == -1);

            tuples[i].data.val.b = num;
            tuples[i].data.type = typeDouble;

        } else if (inputT[j] == 's') {

            char stringInput[SIZE];
            puts("Enter String Value:");
            scanf("%s", stringInput);
            printf("Entered: %s\n", stringInput);

            tuples[i].data.type = typeString;
            strcpy(tuples[i].data.val.s, stringInput);

        } else {
            printf("ERROR: Invalid Type\nd\tINT\nc\tCHAR\nl\tlong\nf\tFLOAT\nb\tDOUBLE\n");
            funcStatus(-1);
        }
        j++;
    }
    lengthTuples = lengthTuples + tupleSize;

    if (overwriteFlag == 0) {
        AmountTuples++;
        printf("Tuple [%d] created\n", (AmountTuples));
    }


}


void showTuple(tuple_t *ptr) {
    if (ptr != NULL) {
        int tupleHead = ptr->head;
        int tupleSize = ptr->size;
        printf("Showing Tuple:\n");
        printf("ID:\t[%s]\t\n", ptr->id);

        for (int i = tupleHead; i < tupleHead + tupleSize; ++i) {
            printTuple(i);
        }
    } else { printf("NULL POINTER ERROR: nothing to show\n"); }
}


tuple_t *getTupleByID(char *id) {
    int tupleCount = lengthTuples;
    int index = 0;

    while (index < tupleCount) {
        if (strcmp(id, tuples[index].id) == 0) {
            return &tuples[index];
        } else { index++; }
    }
    return NULL;//Returns NULL if it doesnt exist
}

char *getTupleID(tuple_t *tuple_ptr) {

    if (tuple_ptr != NULL) {
        static char ID[SIZE];
        strcpy(ID, tuple_ptr->id);
        return ID;
    } else {
        return NULL;
    }
}

void listTuples() {
    int tupleCount = lengthTuples;

    for (int j = 0; j < tupleCount; ++j) {
        if (strcmp(tuples[j].id, tuples[j - 1].id) != 0) {
            printf("\n");
        }
        printf("%s: ", tuples[j].id);

        printTuple(j);
    }
}

void deleteTuple(char *id) {
    int countTuples = lengthTuples;

    tuple_t *tuple_ptr = getTupleByID(id);

    if (tuple_ptr == NULL) {
        printf("Tuple Deletion Error: Tuple ID [%s] not found.\n", id);
    } else {
        int size = tuple_ptr->size;
        long int newSize = countTuples - ((tuple_ptr + size) - tuples);

        //Memory Shifting so loginc of program remains intact
        lengthTuples -= size; // decrement tuples array size by size of deleted tuple
        memmove(tuple_ptr, tuple_ptr + size, newSize * sizeof(tuple_t));
        tuples = realloc(tuples, lengthTuples * sizeof(tuple_t));
        AmountTuples--;
    }
}


void joinTuples(char id[SIZE], tuple_t *ptr1, tuple_t *ptr2) {
//    int tupleCount = lengthTuples;
    if (ptr1 != NULL && ptr2 != NULL) {

        char NewID[SIZE];
        strcpy(NewID, id);
        int tupleCount = lengthTuples;
        int tupleSize_1 = ptr1->size;
        int tupleSize_2 = ptr2->size;
        int tupleHead_1 = ptr1->head;
        int tupleHead_2 = ptr2->head;

        int tupleSize = tupleSize_1 + tupleSize_2;
        printf("TUPLE SIZE %d\n", tupleSize);
        int j = 0;


        char *inputT = fillTypes(ptr1, ptr2);
        printf("TYPES: %s\n", inputT);

        tuples = realloc(tuples, (tupleCount + tupleSize) * sizeof(tuple_t));
        ptr_valid(tuples);


        int t1 = tupleCount;
        for (int q = tupleHead_1; q < tupleHead_1 + tupleSize_1; ++q) {
            strcpy(tuples[t1].id, NewID);
            tuples[t1].size = tupleSize;
            tuples[t1].head = tupleCount;


            if (inputT[j] == 'd') {
                tuples[t1].data.type = typeDigit;
                tuples[t1].data.val.d = tuples[q].data.val.d;

            } else if (inputT[j] == 'c') {
                tuples[t1].data.type = typeChar;
                tuples[t1].data.val.c = tuples[q].data.val.c;
            } else if (inputT[j] == 'l') {
                tuples[t1].data.type = typeLong;
                tuples[t1].data.val.l = tuples[q].data.val.l;

            } else if (inputT[j] == 'f') {
                tuples[t1].data.type = typeFloat;
                tuples[t1].data.val.f = tuples[q].data.val.f;

            } else if (inputT[j] == 'b') {
                tuples[t1].data.type = typeDouble;
                tuples[t1].data.val.b = tuples[q].data.val.b;

            } else if (inputT[j] == 's') {
                tuples[t1].data.type = typeString;
                strcpy(tuples[t1].data.val.s, tuples[q].data.val.s);

            } else { printf("lol no no \n"); }
            t1++;
            j++;
        }

        int t2 = tupleCount + tupleSize_1;
        for (int q = tupleHead_2; q < tupleHead_2 + tupleSize_2; ++q) {

            strcpy(tuples[t2].id, NewID);
            tuples[t2].size = tupleSize;
            tuples[t2].head = tupleCount;

            if (inputT[j] == 'd') {
                tuples[t2].data.type = typeDigit;
                tuples[t2].data.val.d = tuples[q].data.val.d;

            } else if (inputT[j] == 'c') {
                tuples[t2].data.type = typeChar;
                tuples[t2].data.val.c = tuples[q].data.val.c;

            } else if (inputT[j] == 'l') {
                tuples[t2].data.type = typeLong;
                tuples[t2].data.val.l = tuples[q].data.val.l;

            } else if (inputT[j] == 'f') {
                tuples[t2].data.type = typeFloat;
                tuples[t2].data.val.f = tuples[q].data.val.f;

            } else if (inputT[j] == 'b') {
                tuples[t2].data.type = typeDouble;
                tuples[t2].data.val.b = tuples[q].data.val.b;

            } else if (inputT[j] == 's') {
                tuples[t2].data.type = typeString;
                strcpy(tuples[t2].data.val.s, tuples[q].data.val.s);

            } else { error(t2); }
            j++;
            t2++;
        }

        lengthTuples = lengthTuples + tupleSize;
        AmountTuples++;
        printf("Tuple [%d] created\n", (AmountTuples));

        deleteTuple(getTupleID(ptr2));
        deleteTuple(getTupleID(ptr1));
    } else {
        printf("TUPLE POINTER ERROR: Join Aborting\n");
        funcStatus(-1);
    }
}


int cmpTuples(tuple_t *ptr1, tuple_t *ptr2) {

    if (ptr1 != NULL && ptr2 != NULL) {

        int CMP = 0;
        int head = 0;
        int size = 0;

        char *tupleVar_1 = fillTuple(ptr1);
        char *tupleVar_2 = fillTuple(ptr2);

        bool t2Prefix = startsWith(tupleVar_2, tupleVar_1); //Tuple2 is a prefix of Tuple1
        bool t1Prefix = startsWith(tupleVar_1, tupleVar_2); //Tuple1 is a prefix of Tuple2




        if (strcmp(tupleVar_1, tupleVar_2) == 0) {
            //If both tuples are identical then they can be compared equally
            head = ptr1->head;
            size = ptr1->size;
        } else if ((strcmp(tupleVar_1, tupleVar_2) > 0) && (t2Prefix == true)) {
            //if t2 is less than and is a prefix of t1
            head = ptr1->head;   //Start comparing t1 to t2 until size of smaller t2 is reached
            size = ptr2->size;
        } else if ((strcmp(tupleVar_1, tupleVar_2) < 0) && (t1Prefix == true)) {
            //if t1 is less than and is a prefix of t2
            head = ptr1->size; //Start comparing t2 to t1 until size of smaller t1 is reached
            size = ptr1->size;
        } else {
            error(1);
            return 10;
        }


        int j = ptr2->head;
        for (int i = head; i < head + size; ++i) {
            if (tuples[i].data.type == typeDigit) {

                int intVal_1 = tuples[i].data.val.d;
                int intVal_2 = tuples[j].data.val.d;

                if (intVal_1 > intVal_2) {
                    return 1;
                } else if (intVal_1 < intVal_2) {
                    return -1;
                } else { CMP = 0; }

            } else if (tuples[i].data.type == typeChar) {

                char charVal_1 = tuples[i].data.val.c;
                char charVal_2 = tuples[j].data.val.c;

                if (charVal_1 > charVal_2) {
                    return 1;
                } else if (charVal_1 < charVal_2) {
                    return -1;
                } else { CMP = 0; }

            } else if (tuples[i].data.type == typeLong) {

                long longVal_1 = tuples[i].data.val.l;
                long longVal_2 = tuples[j].data.val.l;

                if (longVal_1 > longVal_2) {
                    return 1;
                } else if (longVal_1 < longVal_2) {
                    return -1;
                } else { CMP = 0; }

            } else if (tuples[i].data.type == typeFloat) {

                float floatVal_1 = tuples[i].data.val.f;
                float floatVal_2 = tuples[j].data.val.f;

                if (floatVal_1 > floatVal_2) {
                    return 1;
                } else if (floatVal_1 < floatVal_2) {
                    return -1;
                } else { CMP = 0; }


            } else if (tuples[i].data.type == typeDouble) {

                double doubleVal_1 = tuples[i].data.val.b;
                double doubleVal_2 = tuples[j].data.val.b;

                if (doubleVal_1 > doubleVal_2) {
                    return 1;
                } else if (doubleVal_1 < doubleVal_2) {
                    return -1;
                } else { CMP = 0; }

            } else if (tuples[i].data.type == typeString) {
                int strVal = strcmp(ptr1->data.val.s, ptr2->data.val.s);
                if (strVal > 0) {
                    return 1;
                } else if (strVal < 0) {
                    return -1;
                } else { CMP = 0; }
            }
            j++;
        }

        if (CMP == 0) {
            return 0;
        }

    } else {
        error(1);
    }
    return 10;
}


void printTuple(int i) {

    if (tuples[i].data.type == typeDigit) {
        printf("INT: \t%d\n", tuples[i].data.val.d);
    } else if (tuples[i].data.type == typeChar) {
        printf("CHAR:\t%c\n", tuples[i].data.val.c);
    } else if (tuples[i].data.type == typeLong) {
        printf("LONG:\t%ld\n", tuples[i].data.val.l);
    } else if (tuples[i].data.type == typeFloat) {
        printf("FLOAT:\t%f\n", tuples[i].data.val.f);
    } else if (tuples[i].data.type == typeDouble) {
        printf("DOUBLE:\t%lf\n", tuples[i].data.val.b);
    } else if (tuples[i].data.type == typeString) {
        printf("STRING:\t%s\n", tuples[i].data.val.s);
    } else {
        printf("nothing this round[%d]\n", i);
    }

}

void saveAllTuples() {
    int tupleCount = lengthTuples;
    FILE *varPointer_t = fopen("Vars.txt", "w");
    FILE *dataPointer_t = fopen("Data.txt", "w");
    int i = 0;

    while (i != tupleCount) {
        int tupleSize = tuples[i].size;
        char *TupleVar = fillTuple(getTupleByID(tuples[i].id));
        fprintf(varPointer_t, "%s\t%d\t%s\n", tuples[i].id, tupleSize, TupleVar);
        i += tupleSize;
    }

    for (int j = 0; j < tupleCount; ++j) {
        if (tuples[j].data.type == typeDigit) {
            fprintf(dataPointer_t, "%d\n", tuples[j].data.val.d);
        } else if (tuples[j].data.type == typeChar) {
            fprintf(dataPointer_t, "%c\n", tuples[j].data.val.c);
        } else if (tuples[j].data.type == typeLong) {
            fprintf(dataPointer_t, "%ld\n", tuples[j].data.val.l);
        } else if (tuples[j].data.type == typeFloat) {
            fprintf(dataPointer_t, "%f\n", tuples[j].data.val.f);
        } else if (tuples[j].data.type == typeDouble) {
            fprintf(dataPointer_t, "%lf\n", tuples[j].data.val.b);
        } else if (tuples[j].data.type == typeString) {
            fprintf(dataPointer_t, "%s\n", tuples[j].data.val.s);
        } else {
            printf("nothing this round[%d]\n", j);
        }
    }

    fclose(varPointer_t);
    fclose(dataPointer_t);
}

void loadAllTuples() {

    FILE *varPointer_t = fopen("Vars.txt", "r");
    FILE *dataPointer_t = fopen("data.txt", "r");

    if (varPointer_t != NULL && dataPointer_t != NULL) {
        while (!feof(varPointer_t)) {
            int tupleCount = lengthTuples;
            char varType[SIZE];
            char id[SIZE];
            int tupleSizeP = 0;
            int j = 0;

            fscanf(varPointer_t, "%s\t%d\t%s", id, &tupleSizeP, varType);
            if (tupleSizeP != 0) {
                printf("VARTYPE:%s\t", varType);
                printf("ID:%s\t", id);
                printf("tupleSize:%d\t\n", tupleSizeP);
                int tupleSize = tupleSizeP;


                tuples = realloc(tuples, (tupleCount + tupleSize) * sizeof(tuple_t));
                ptr_valid(tuples);

                for (int i = tupleCount; i < tupleCount + tupleSize; ++i) {
                    strcpy(tuples[i].id, id);
                    tuples[i].size = tupleSize;
                    tuples[i].head = tupleCount;

                    if (varType[j] == 'd') {
                        tuples[i].data.type = typeDigit;
                        fscanf(dataPointer_t, "%d\n", &tuples[i].data.val.d);
                    } else if (varType[j] == 'c') {
                        fscanf(dataPointer_t, "%c\n", &tuples[i].data.val.c);
                        tuples[i].data.type = typeChar;
                    } else if (varType[j] == 'l') {
                        fscanf(dataPointer_t, "%ld\n", &tuples[i].data.val.l);
                        tuples[i].data.type = typeLong;
                    } else if (varType[j] == 'f') {
                        tuples[i].data.type = typeFloat;
                        fscanf(dataPointer_t, "%f\n", &tuples[i].data.val.f);
                    } else if (varType[j] == 'b') {
                        tuples[i].data.type = typeDouble;
                        fscanf(dataPointer_t, "%lf\n", &tuples[i].data.val.b);
                    } else if (varType[j] == 's') {
                        char stringInput[SIZE];
                        fscanf(dataPointer_t, "%s\n", stringInput);
                        tuples[i].data.type = typeString;
                        strcpy(tuples[i].data.val.s, stringInput);
                    } else { printf("ERROR: Invalid Type\nd\tINT\nc\tCHAR\nl\tlong\nf\tFLOAT\nb\tDOUBLE\n"); }
                    j++;
                }
                lengthTuples = lengthTuples + tupleSize;
                AmountTuples++;
                printf("Tuple [%d] created\n", (AmountTuples));

            } else {
                printf("End of File\n");
            }
        }

        fclose(varPointer_t);
        fclose(dataPointer_t);
    } else {
        printf("LOAD TUPLE ERROR: Locating files has failed. Could be no tuples have been saved yet!\n");
        funcStatus(-1);
    }
}

char *fillTuple(tuple_t *ptr1) {
    int k = 0;
    static char tupleVar[SIZE];

    for (int i = ptr1->head; i < ptr1->head + ptr1->size; ++i) {
        if (tuples[i].data.type == typeDigit) {
            strcpy(&tupleVar[k], "d");
        } else if (tuples[i].data.type == typeChar) {
            strcpy(&tupleVar[k], "c");
        } else if (tuples[i].data.type == typeLong) {
            strcpy(&tupleVar[k], "l");
        } else if (tuples[i].data.type == typeFloat) {
            strcpy(&tupleVar[k], "f");
        } else if (tuples[i].data.type == typeDouble) {
            strcpy(&tupleVar[k], "b");
        } else if (tuples[i].data.type == typeString) {
            strcpy(&tupleVar[k], "s");
        } else {
            error(i);
        }
        k++;
    }
    return tupleVar;
}

char *fillTypes(tuple_t *ptr1, tuple_t *ptr2) {
    int k = 0;
    static char tupleVar[SIZE];

    for (int i = ptr1->head; i < ptr1->head + ptr1->size; ++i) {
        if (tuples[i].data.type == typeDigit) {
            strcpy(&tupleVar[k], "d");
        } else if (tuples[i].data.type == typeChar) {
            strcpy(&tupleVar[k], "c");
        } else if (tuples[i].data.type == typeLong) {
            strcpy(&tupleVar[k], "l");
        } else if (tuples[i].data.type == typeFloat) {
            strcpy(&tupleVar[k], "f");
        } else if (tuples[i].data.type == typeDouble) {
            strcpy(&tupleVar[k], "b");
        } else if (tuples[i].data.type == typeString) {
            strcpy(&tupleVar[k], "s");
        } else {
            error(i);
        }
        k++;
    }

    for (int i = ptr2->head; i < ptr2->head + ptr2->size; ++i) {
        if (tuples[i].data.type == typeDigit) {
            strcpy(&tupleVar[k], "d");
        } else if (tuples[i].data.type == typeChar) {
            strcpy(&tupleVar[k], "c");
        } else if (tuples[i].data.type == typeLong) {
            strcpy(&tupleVar[k], "l");
        } else if (tuples[i].data.type == typeFloat) {
            strcpy(&tupleVar[k], "f");
        } else if (tuples[i].data.type == typeDouble) {
            strcpy(&tupleVar[k], "b");
        } else if (tuples[i].data.type == typeString) {
            strcpy(&tupleVar[k], "s");
        } else {
            error(i);
        }
        k++;
    }

    return tupleVar;
}


char *formatInput(char userInput[SIZE]) {
    int i = 2;
    int j = 0;
    static char inter[SIZE];

    while (userInput[j] != '\0') {
        inter[j] = userInput[i];
        j++;
        i += 2;
    }


    char *final = removeLastChar(inter);

    return final;
}


char *removeLastChar(char *string) {
    static char inter[SIZE];
    strcpy(inter, string);

    int length = strlen(inter);//Get length of string
    inter[length - 1] = '\0';//Set next to last char to null


    return inter;
}

int sizeInit(char *input) {
    char size;
    size = input[0];
    int sizeVal = atoi(&size);
    return sizeVal;
}

bool startsWith(const char *pre, const char *str) {
    size_t lenpre = strlen(pre),
            lenstr = strlen(str);
    return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
}

void error(int i) {
    printf("\nERROR in round [%d]\n", i);
}

void funcStatus(int i) {
    if (i == -1) {
        exit(EXIT_FAILURE);
    } else if (i == 1) {
        exit(EXIT_SUCCESS);
    } else {
        exit(0);
    }
}


void ptr_valid(void *ptr) {

    if (ptr == NULL) {
        printf("MEMORY ALLOCATION FAILED: Memory allocation failed during a realloc\n");
        funcStatus(-1);
    }

}

void terminate()
{
    free(tuples);
}
