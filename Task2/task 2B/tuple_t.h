//tuple_t.h header file for the tuple data type
#ifndef TUPLEE_TUPLE_T_H
#define TUPLEE_TUPLE_T_H
#include <stdio.h>
#include <stdlib.h>
#include <mem.h>
#include <ctype.h>
#include <stdbool.h>

#define SIZE 256

union val {
    int d;
    char c;
    long l;
    float f;
    double b;
    char s[SIZE];
};

typedef enum type {
    d, l, f, b, c, s
} typed;

typedef struct {
    union val val;
    enum type type;
} vars;

typedef struct {
    char id[SIZE];
    vars data;
    int head;
    int size;
} tuple_t;

/*opertation:  Create a tuple*/
/*precondition:  A tuple pointer is created to store them E.G (tuple_t *tuples;) */
/*postcondition:  tuples is empty initially*/
void createTuple(char id[SIZE], char *userInput);

/*opertation: Shows contents of tuple    */
/*precondition:   Tuple pointer is loaded/known (E.G: through getTupleByID())*/
/*postcondition:  Tuple will be returned if exists else returns NULL*/
void showTuple(tuple_t *ptr);

/*opertation:  Returns Tuple Pointer*/
/*precondition:  TupleID is known*/
/*postcondition:  Returns tuple pointer else NULL*/
tuple_t *getTupleByID(char *id);

/*opertation:  Create a tuple*/
/*precondition:  tuple pointer is knows/loaded*/
/*postcondition:  Returns ID else if does't exist NULL*/
char *getTupleID(tuple_t *tuple_ptr);

/*opertation:  Deletes tuple*/
void deleteTuple(char *id);

/*opertation:  Join 2 tuples to new Tuple*/
/*precondition:  tuple pointers is knows/loaded*/
/*postcondition:  Both tuple will beomce one tuple with provided ID's*/
void joinTuples(char id[SIZE], tuple_t *ptr1, tuple_t *ptr2);

/*opertation:  Returns 1/-1/0 by comparing tuples*/
/*precondition:  tuple pointers is knows/loaded*/
/*postcondition:  Return 0 if both tuples are equal
 * else  returns 1 if t1's first non equal
 * value is bigger and -1 if vice-versa*/
int cmpTuples(tuple_t *ptr1, tuple_t *ptr2);

/*opertation:Saves Tuple to a files*/
void saveAllTuples();

/*opertation:  Loads tuples*/
/*precondition:  Files with values exists in correct format*/
void loadAllTuples();

void listTuples();

void terminate();



#endif //TUPLEE_TUPLE_T_H

