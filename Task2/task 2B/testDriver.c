#include "tuple_t.h"


int main(void) {
    //Creating 2 Tuples
    createTuple("1", "3:s:c:d");
    createTuple("2", "3:s:c:d");

    //Saving Both Tuples and deleting one
    saveAllTuples();
    deleteTuple("1");
    deleteTuple("2");

    printf("\n");
    //Loading and Showing the deleted Tuple
    loadAllTuples();
    showTuple(getTupleByID("1"));
    printf("\n");

    createTuple("3", "3:s:c:d");
    createTuple("4", "3:s:c:d");
    createTuple("5", "3:s:c:d");

    //Joing Tuple 3 and Tuple 4 to new Tuple 6 and showing it
    printf("\nJOINING 3&4 to 6\n");
    joinTuples("6", getTupleByID("3"), getTupleByID("4"));
    printf("Tuples Joined\n");
    listTuples();
    printf("\n");

    //Comparing tuple 1 TO 2   and 5 TO 6
    int cmp = cmpTuples(getTupleByID("1"), getTupleByID("2"));
    int cmp1 = cmpTuples(getTupleByID("5"), getTupleByID("6"));
    printf("Tuple Compare[1&2] Return = %d\n", cmp);
    printf("Tuple Compare[5&6] Return = %d\n", cmp1);

    printf("\n");
    //Showing Get Tuple By ID function
    printf("---------------------\n");
    printf("Tuple ID:1\n");
    showTuple(getTupleByID("1"));
    printf("---------------------\n");

    //Show GetTupleID function
    char *IDloc = getTupleID(getTupleByID("6"));
    printf("Should show 6: [%s]\n", IDloc);


    //Deleting and showing Tuple  Should print Error
    printf("\n---------------------\n");
    printf("Deleting Tuple 6\n");
    deleteTuple("6");
    printf("Showing tuple. Should print Error:\n");
    showTuple(getTupleByID("6"));

    terminate();
    return 0;
}

