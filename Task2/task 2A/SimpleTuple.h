//This is the simplified tuple defenition for task 2A.
// Created by Zakkarija Micallef on 24/12/2018.
//

#ifndef C_JAN_TUPLE_T_SIMPLETUPLE_H
#define C_JAN_TUPLE_T_SIMPLETUPLE_H
#define SIZE 256

#include <mem.h>
#include <stdlib.h>
#include <stdio.h>


typedef struct {
    char id[SIZE];
    char strVal[SIZE];
    int intVal;
} simpleTuple;

void createTuple(char id[SIZE], char *strVal, int intVal);

simpleTuple *getTupleByID(char *id);

char *getTupleID(simpleTuple *tuple_ptr);

void showTuple(simpleTuple *tuple_ptr);

void listTuples(void);

void deleteTuple(char *id);

int cmpTuples(char id_1[SIZE], char id_2[SIZE]);

void joinTuple(simpleTuple *tuple_ptr_1, simpleTuple *tuple_ptr_2);

void saveAllTuples();

void loadAllTuples();



#endif //C_JAN_TUPLE_T_SIMPLETUPLE_H
