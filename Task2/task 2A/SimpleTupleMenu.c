#include "SimpleTuple.h"

simpleTuple *tuples;

int main() {

    int opt = 0;

    do {
        printf(" 1.Create Tuple\n 2.Get Tuple By ID\n 3.Get ID of Tuple\n 4.Show Tuple\n");
        printf(" 5.List Tuple\n 6.Compare Tuple\n 7.Join Tuple\n 8.Save All tuple\n 9.Load All Tuples\n 0.Exit\n");
        printf("-----------------Choose opt-----------------\n");
        scanf("%d", &opt);

        switch (opt) {
            case 1: {
                int wage;
                char id[SIZE];
                char name[SIZE];
                puts("-----------------");
                puts("Create Tuple");
                puts("Enter ID:");
                scanf("%s", id);
                printf("Enter Name:\n");
                scanf("%s", name);
                printf("Enter Wage:\n");
                scanf("%d", &wage);
                createTuple(id, name, wage);
                puts("TUPLE CREATED");
            }
                break;
            case 2: {
                puts("-----------------");
                puts("Get Tuple By ID");
                char id[SIZE];
                printf("Enter ID");
                gets(id);
                showTuple(getTupleByID(id));
            }
                break;
            case 3: {
                puts("-----------------");
                puts("Get ID of Tuple");
                simpleTuple *tuple_ptr;
                printf("Enter Tuple pointer:\n");
                scanf("%p", &tuple_ptr);
                getTupleID(tuple_ptr);
            }
                break;
            case 4: {
                puts("-----------------");
                puts("Show Tuple");
                simpleTuple *tuple_ptr;
                printf("Enter Tuple pointer:\n");
                scanf("%p", &tuple_ptr);
                showTuple(tuple_ptr);
            }
                break;
            case 5:
                puts("Tuples");
                puts("-----------------");
                listTuples();
                puts("");
                break;
            case 6: {
                puts("-----------------");
                puts("Compare Tuples");
                char id1[SIZE];
                char id2[SIZE];
                printf("Enter first ID to compare");
                gets(id1);
                printf("Enter second ID to compare");
                gets(id2);

                cmpTuples(id1, id2);
            }
                break;
            case 7: {
                puts("-----------------");
                puts("Join Tuples");

                simpleTuple *tuple_ptr_1;
                simpleTuple *tuple_ptr_2;
                char ID_1[SIZE];
                char ID_2[SIZE];
                printf("Enter first Tuple ID:\n");
                scanf("%s", ID_1);
                tuple_ptr_1 = getTupleByID(ID_1);
                printf("Enter second Tuple ID:\n");
                scanf("%s", ID_2);
                tuple_ptr_2 = getTupleByID(ID_2);

                joinTuple(tuple_ptr_1, tuple_ptr_2);
            }
                break;

            case 8:
                puts("-----------------");
                puts("Save All Tuples");
                saveAllTuples();
                break;

            case 9:
                puts("-----------------");
                puts("Load All Tuples");
                loadAllTuples();
                break;

            default:
                if (opt == 0) { break; }
                else {
                    puts("Bye! Bye!");
                }
        }
    } while (opt != 0);

    free(tuples);
    return 0;
}
