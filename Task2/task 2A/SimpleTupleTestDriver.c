//Created by Zakkarija Micallef
//This is test Driver. Choose "testDriver" and run it.
//Everything will be done automatically while trying to show every required component
#include "SimpleTuple.h"

simpleTuple *tuples;

int main() {

    loadAllTuples();
    createTuple("0466500L", "Zakkarija", 500);
    createTuple("0455960L", "Sara", 200);
    createTuple("7654321L", "Maria", 2501);
    createTuple("1234567L", "Jason", 50);
    createTuple("0466500L", "Neil", 5123);
    createTuple("0562142L", "Esther", 100);

    puts("");
    printf("JoinTuple function\nJoining\n");
    showTuple(getTupleByID("0466500L"));
    printf("&\n");
    showTuple(getTupleByID("1234567L"));
    joinTuple(getTupleByID("0466500L"), getTupleByID("1234567L"));
    puts("");
    puts("ShowTuple Function:");
    showTuple(getTupleByID("0466500L"));
    printf("\n\n");

    puts("getTupleID Function:");
    char *id = getTupleID(getTupleByID("0455960L"));
    printf(" Should show Number 0455960L: [%s]\n", id);
    printf("\n\n");

    createTuple("1", "A", 1000);
    createTuple("2", "A", 2000);
    int x = cmpTuples("1", "2");
    printf("Should show -1 since 2000>1000 [%d]\n", x);

    deleteTuple("1");
    deleteTuple("2");
    printf("\n");
    printf("ShowTuple of ID[1] Should show Pointer Error\n");
    showTuple(getTupleByID("1"));

    saveAllTuples();
    free(tuples);
    printf("\n----------------------\nEnded\n----------------------");

    return 0;
}
