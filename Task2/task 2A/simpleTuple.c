#include "SimpleTuple.h"

simpleTuple *tuples;
int sizeTuples = 0;


void createTuple(char id[SIZE], char *strVal, int intVal) {
    int tupleCount = sizeTuples;
//    simpleTuple *currentTuple_ptr;

    if (getTupleByID(id) != NULL && tupleCount >= 1) {
        printf("\n\nID already exists\nOVERWRITTING [%s]: %s\n", id, strVal);
        //Creates Tuple Heap Space
        tuples = realloc(tuples, (tupleCount + 3) * sizeof(simpleTuple));

        //Maps variables to the Tuple
        printf("[%s] Adding '%s', mapped to %d\n", id, strVal, intVal);
        strcpy(tuples[tupleCount].id, id);
        strcpy(tuples[tupleCount].strVal, strVal);
        tuples[tupleCount].intVal = intVal;

        sizeTuples++;
        deleteTuple(id);
    } else {
        //Creates Tuple Heap Space
        tuples = realloc(tuples, (tupleCount + 3) * sizeof(simpleTuple));

        //Maps variables to the Tuple
        printf("[%s] Adding '%s', mapped to %d\n", id, strVal, intVal);
        strcpy(tuples[tupleCount].id, id);
        strcpy(tuples[tupleCount].strVal, strVal);
        tuples[tupleCount].intVal = intVal;

        sizeTuples++;
    }
}


void showTuple(simpleTuple *tuple_ptr) {
    if (tuple_ptr != NULL) {

        printf("%s:\t[%s] -> %d\n", tuple_ptr->id, tuple_ptr->strVal, tuple_ptr->intVal);

    } else {
        printf("POINTER ERROR: Tuple may not exist\n");
    }
}

simpleTuple *getTupleByID(char *id) {
    int tupleCount = sizeTuples;
    int index = 0;

    while (index < tupleCount) {
        if (strcmp(id, tuples[index].id) == 0) {
            return &tuples[index];
        } else { index++; }
    }
    return NULL;//Returns NULL if it doesnt exist
}


char *getTupleID(simpleTuple *tuple_ptr) {
    if (tuple_ptr != NULL) {
        static char ID[SIZE];
        strcpy(ID, tuple_ptr->id);

        return ID;
    } else {
        return NULL;
    }
}

void deleteTuple(char *id) {
    int tupleCount = sizeTuples;
    int index = 0;

    while (index < tupleCount) {
        if (strcmp(id, tuples[index].id) == 0) {

            printf("Deleting '%s', mapped to %s %d\n", id, tuples[index].strVal, tuples[index].intVal);

            if (index != tupleCount - 1) {
                strcpy(tuples[index].id, tuples[tupleCount - 1].id);
                strcpy(tuples[index].strVal, tuples[tupleCount - 1].strVal);
                tuples[index].intVal = tuples[tupleCount - 1].intVal;
            }
            --sizeTuples;
            break;
        } else { index++; }
    }
    if (index == tupleCount) { printf("\nERROR during DELETION: Tuple with ID not found\n"); }
}


void listTuples(void) {
    int tupleCount = sizeTuples;

    printf("==========\nTuple count is %d\n", tupleCount);
    for (int i = 0; i < tupleCount; ++i) {
        printf("[%s]:\t%s -> %d\n", tuples[i].id, tuples[i].strVal, tuples[i].intVal);
    }
}

int cmpTuples(char id_1[SIZE], char id_2[SIZE]) {
    simpleTuple *tuple1 = getTupleByID(id_1);
    simpleTuple *tuple2 = getTupleByID(id_2);

    int CMP; //Used as Flags


//    if (strcmp(tuple1->id, tuple2->id) > 0) {
//        return 1;
//    } else if (strcmp(tuple1->id, tuple2->id) < 0) {
//        return -1;
//    } else { CMP = 0; }

    if (strcmp(tuple1->strVal, tuple2->strVal) > 0) {
        return 1;
    } else if (strcmp(tuple1->strVal, tuple2->strVal) < 0) {
        return -1;
    } else { CMP = 0; }

    if (tuple1->intVal > tuple2->intVal) {
        return 1;
    } else if (tuple1->intVal < tuple2->intVal) {
        return -1;
    } else { CMP = 0; }

    if (CMP == 0) {
        return 0;
    }
    return 10;
}

void joinTuple(simpleTuple *tuple_ptr_1, simpleTuple *tuple_ptr_2) {

    strcpy(tuple_ptr_1->strVal, tuple_ptr_2->strVal);
    tuple_ptr_1->intVal = tuple_ptr_2->intVal;
    deleteTuple(getTupleID(tuple_ptr_2));
    printf("Tuple of address [%p] joined/copied to address of [%p]\n", tuple_ptr_1, tuple_ptr_2);

}

void saveAllTuples() {

    int tupleCount = sizeTuples;
    FILE *FPointer_t = fopen("tuples.txt", "w");

    for (int i = 0; i < tupleCount; ++i) {
        fprintf(FPointer_t, "%s\t%s\t%d\n", tuples[i].id, tuples[i].strVal, tuples[i].intVal);
    }
    printf("Tuples have been saved\n");

    fclose(FPointer_t);
}

void loadAllTuples() {

    FILE *FPointer_t = fopen("tuples.txt", "r");

    if (FPointer_t != NULL) {
        while (!feof(FPointer_t)) {
            int fIntVal;
            char fstrVal[SIZE];
            char fid[SIZE];
            fscanf(FPointer_t, "%s\t%s\t%d\n", fid, fstrVal, &fIntVal);
            createTuple(fid, fstrVal, fIntVal);
        }
        fclose(FPointer_t);
    } else {
        printf("TUPLE LOADING ERROR: File may not exist\n");
    }
}

