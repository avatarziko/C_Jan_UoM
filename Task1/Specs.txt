1. Problem solving. (Total-45 marks)
For each of the following tasks, the allocated marks are equally split
between:

    • correct application of programming principles;
    • source code explanation; and
    • working solution as demonstrated for all tasks through task (c).


Tasks: -

    (a) Write a function that makes a copy of the contents of a 3D array
    of integers. The function should support any 3D array size.
    [15 marks]

    (b) Write a function that takes an array of strings as argument and reverses the order of individual words in each string. For e.g. the array {"This is a first sentence", "and this is a second"}
    is transformed to {"sentence first a is This", "second a is
    this and"}.
    [15 marks]

    (c) Write a program that presents the end-user with a commandline menu, and that repeatedly asks the user to execute either of
    the functions above, or to quit. The program should prompt for
    function arguments accordingly. Proper validation of user input
    is expected.
    [15 marks]