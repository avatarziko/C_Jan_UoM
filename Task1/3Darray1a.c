#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void arCpy(int width,int height,int depth,int arrayNum[width][height][depth]);

int main() {
    int width, height, depth;

    printf("Enter Width Height Depth");
    scanf("%d %d %d",&width, &height, &depth);

    int arrayNum[width][height][depth];

    for (int k = 0; k < width; ++k) {
        for ( int i = 0; i < depth; ++i ) {
            for (int j = 0; j < height; ++j){
                arrayNum[k][i][j] = rand() % 100;
            }
        }
    }


    arCpy(width,height,depth,arrayNum);

    return 0;
}

void arCpy(int width,int height,int depth,int arrayNum[width][height][depth]){

    int arrayNumCpy[width][height][depth];

    for (int k = 0; k < width; ++k) {
        for (int i = 0; i < depth; ++i ) {
            printf("\n");
            for (int j = 0; j < height; ++j)
                printf("%5d", arrayNum[k][i][j]);
        }
    }
    printf("\n Original Array Address: %p", arrayNum);

    printf("\n\nThis is array Copy\n");

    for (int k = 0; k < width; ++k){
        for (int i = 0; i < depth; ++i) {
            for (int j = 0; j < height; ++j) {
                memcpy(&arrayNumCpy[k][i][j], &arrayNum[k][i][j], sizeof(arrayNum[width][depth][height]));
            }
        }
    }

    for (int k = 0; k < width; ++k) {
        for ( int i = 0; i < depth; ++i ) {
            printf("\n");
            for (int j = 0; j < height; ++j)
                printf("%5d", arrayNumCpy[k][i][j]);
        }
    }
    printf("\nCopied Array Address : %p", arrayNumCpy);
}



