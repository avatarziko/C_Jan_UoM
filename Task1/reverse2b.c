#include <stdio.h>
#include <string.h>
#define MAX 256

void reverseString(char senOriginal[MAX]);

int main() {

    char senOriginal[MAX];

    puts("Enter String to reverse");
    gets(senOriginal);

    reverseString(senOriginal);

    return 0;
}

void reverseString(char senOriginal[MAX]){
    char sen[MAX], rev[MAX], random[5];
    int len, index, wordStart, wordEnd;

    strcpy(random, "hello ");
    int i; //counter

    strcat(sen, random);
    strcat(sen, senOriginal);

    len = strlen(sen);
    index = 0;

    // check for word from the end of string, -since null
    wordStart = len - 1;
    wordEnd = len - 1;

    do {
        // If a word is found
        if (sen[wordStart] == ' ') {
            // Add the word to the rev string
            i = wordStart + 1;
            while (i <= wordEnd) {
                rev[index] = sen[i];

                i++;
                index++;
            }
            rev[index++] = ' ';

            wordEnd = wordStart - 1;
        }
        wordStart--;
    } while (wordStart > 0);

    // Add NULL character at the end of rev string
    rev[index] = '\0';

    printf("\n\n");
    printf("Reversed String:\n %s", rev);
}