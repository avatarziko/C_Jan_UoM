//
// Created by Zak Micallef on 12/01/2019.
//

#ifndef ASSIGMENT1_MAIN_H
#define ASSIGMENT1_MAIN_H


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 256

void arCpy(int width, int height, int depth, int arrayNum[width][height][depth]);

void reverseString(char senOriginal[MAX]);


#endif //ASSIGMENT1_MAIN_H
