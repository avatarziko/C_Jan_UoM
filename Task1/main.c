#include "main.h"

// Created by Zak Micallef

int main(void) {

    int width, height, depth;
    char senOriginal[MAX];
    int opt = 0;

    printf("Hello\n");

    do {
        printf(" 1.Copy 3d Array\n 2.Reverse a String\n 0.Exit");
        printf("\nChoose opt:");
        scanf("%d", &opt);
        fflush(stdin);
        switch (opt) {
            case 1: {
                puts("Copy 3d Array of integers");
                puts("Enter Width Height Depth (e.g 10 3 4)");
                scanf("%d %d %d", &width, &height, &depth);

                int arrayNum[width][height][depth];

                //Fills array with random number from 1 - 99
                for (int k = 0; k < width; ++k) {
                    for (int i = 0; i < depth; ++i) {
                        for (int j = 0; j < height; ++j) {
                            arrayNum[k][i][j] = rand() % 100;
                        }
                    }
                }

                //Calls func to copy array and prints them both
                arCpy(width, height, depth, arrayNum);
                break;
            }


            case 2: {
                printf("Enter String to reverse:\n ");
                gets(senOriginal);

                reverseString(senOriginal);
                printf("\n");
                break;
            }

            default:
                puts("Bye");

        }
    } while (opt != 0);

    return 0;
}

void arCpy(int width, int height, int depth, int arrayNum[width][height][depth]) {

    int arrayNumCpy[width][height][depth];

    for (int k = 0; k < width; ++k) {
        for (int i = 0; i < depth; ++i) {
            printf("\n");
            for (int j = 0; j < height; ++j)
                printf("%5d", arrayNum[k][i][j]);
        }
    }
    printf("\n Original Array Address: %p", arrayNum);

    printf("\n\nThis is array Copy\n");

    for (int k = 0; k < width; ++k) {
        for (int i = 0; i < depth; ++i) {
            for (int j = 0; j < height; ++j) {
                memcpy(&arrayNumCpy[k][i][j], &arrayNum[k][i][j], sizeof(arrayNum[width][depth][height]));
            }
        }
    }

    for (int k = 0; k < width; ++k) {
        for (int i = 0; i < depth; ++i) {
            printf("\n");
            for (int j = 0; j < height; ++j)
                printf("%5d", arrayNumCpy[k][i][j]);
        }
    }
    printf("\nAddress : %p\n\n", arrayNumCpy);
}


void reverseString(char senOriginal[MAX]) {
    char sen[MAX], rev[MAX], random[5];
    int len, index, wordStart, wordEnd;

    strcpy(random, "hello ");
    int i; //counter

    strcat(sen, random);
    strcat(sen, senOriginal);

    len = strlen(sen);
    index = 0;

    // check for word from the end of string, -since null
    wordStart = len - 1;
    wordEnd = len - 1;

    do {
        // If a word is found
        if (sen[wordStart] == ' ') {
            // Add the word to the rev string
            i = wordStart + 1;
            while (i <= wordEnd) {
                rev[index] = sen[i];

                i++;
                index++;
            }
            rev[index++] = ' ';

            wordEnd = wordStart - 1;
        }
        wordStart--;
    } while (wordStart > 0);

    // Add NULL character at the end of rev string
    rev[index] = '\0';

    printf("\n\n");
    printf("Reversed String:\n %s \n", rev);
}


